# clogicops

This is a LaTeX package providing graphical symbol macros for the operators of [Giorgize Japaridze's](http://www.csc.villanova.edu/~japaridz/) [Computability Logic](http://www.csc.villanova.edu/~japaridz/CL/).

This package is based heavily off the [original set of macros provided by Japaridze](http://www.csc.villanova.edu/~japaridz/CL/macros.html) some minor positioning/typesetting tweaks by Kevin Buchik.

For a listing of the commands to use for each operator macro, see the file `col_operators_example.pdf`.
